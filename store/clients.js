export const state = () => ({
  loadedClients: null
})

export const mutations = {
  setClients(state, clients) {
    state.loadedClients = clients
  },
  updateClient(state, client) {
    const item = state.loadedClients.find((item) => item.id === client.id)
    Object.assign(item, client)
  }
}

export const actions = {
  fetchClients(vuexContext, context) {
    if (vuexContext.state.loadedClients) {
      return true
    }
    return this.$axios
      .$get('clients/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key] })
        }
        vuexContext.commit('setClients', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setClients(vuexContext, clients) {
    vuexContext.commit('setClients', clients)
  },
  updateClient(vuexContext, client) {
    vuexContext.commit('updateClient', client)
  }
}

export const getters = {
  loadedClients(state) {
    return state.loadedClients
  }
}
