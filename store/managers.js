/* eslint-disable no-console */
export const state = () => ({
  loadedManagers: []
})

export const mutations = {
  setManagers(state, manager) {
    state.loadedManagers = manager
  }
}

export const actions = {
  fetchManagers(vuexContext, context) {
    return this.$axios
      .$get('managers/')
      .then((data) => {
        const managersList = []
        for (const key in data) {
          managersList.push({ ...data[key] })
        }
        vuexContext.commit('setManagers', managersList)
      })
      .catch((e) => context.error(e))
  },
  setManagers(vuexContext, manager) {
    vuexContext.commit('setManagers', manager)
  }
}

export const getters = {
  loadedManagers(state) {
    return state.loadedManagers
  }
}
