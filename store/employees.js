export const state = () => ({
  loadedEmployees: null,
  contractTypes: null
})

export const mutations = {
  setEmployees(state, loadedEmployees) {
    state.loadedEmployees = loadedEmployees
  },
  setContractTypes(state, contractTypes) {
    state.contractTypes = contractTypes
  }
}

export const actions = {
  fetchEmployees(vuexContext, context) {
    if (vuexContext.state.loadedEmployees) {
      return true
    }
    return this.$axios
      .$get('userprofiles/')
      .then((data) => {
        const employeesList = []
        for (const key in data) {
          employeesList.push({ ...data[key] })
        }
        vuexContext.commit('setEmployees', employeesList)
      })
      .catch((e) => context.error(e))
  },
  fetchContractTypes(vuexContext, context) {
    if (vuexContext.state.contractTypes) {
      return true
    }
    return this.$axios
      .$get('contracttypes/')
      .then((data) => {
        const contractTypesList = []
        for (const key in data) {
          contractTypesList.push({ ...data[key] })
        }
        vuexContext.commit('setContractTypes', contractTypesList)
      })
      .catch((e) => context.error(e))
  }
}

export const getters = {
  loadedEmployees(state) {
    return state.loadedEmployees
  },
  loadedContractTypes(state) {
    return state.contractTypes
  }
}
