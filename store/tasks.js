export const state = () => ({
  loadedTasks: []
})

export const mutations = {
  setLoadedTasks(state, loadedTasks) {
    state.loadedTasks = loadedTasks
  },
  updateTask(state, task) {
    const item = state.loadedTasks.find((item) => item.id === task.id)
    Object.assign(item, task)
  },
  addTask(state, task) {
    state.loadedTasks.push(task)
  }
}

export const actions = {
  fetchTasks(vuexContext, context) {
    return this.$axios
      .$get('tasks/')
      .then((data) => {
        const tasksList = []
        for (const key in data) {
          tasksList.push({ ...data[key] })
        }
        vuexContext.commit('setLoadedTasks', tasksList)
      })
      .catch((e) => context.error(e))
  },
  updateTask(vuexContext, task) {
    vuexContext.commit('updateTask', task)
  },
  addTask(vuexContext, task) {
    vuexContext.commit('addTask', { ...task })
  }
}

export const getters = {
  loadedTasks(state) {
    return state.loadedTasks
  }
}
