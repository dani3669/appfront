/* eslint-disable no-console */
export const state = () => ({
  loadedProjectTypes: []
})

export const mutations = {
  setProjectTypes(state, projectType) {
    state.loadedProjectTypes = projectType
  }
}

export const actions = {
  fetchProjectTypes(vuexContext, context) {
    return this.$axios
      .$get('projecttypes/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setProjectTypes', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setProjectTypes(vuexContext, projectType) {
    vuexContext.commit('setProjectTypes', projectType)
  }
}

export const getters = {
  loadedProjectTypes(state) {
    return state.loadedProjectTypes
  }
}
