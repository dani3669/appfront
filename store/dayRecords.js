export const state = () => ({
  workingRecord: []
})

export const mutations = {
  setworkingRecord(state, workingRecord) {
    state.workingRecord = workingRecord
  },
  addDayRecord(state, workingDayRecord) {
    state.workingRecord.push(workingDayRecord)
  },
  updateDayRecord(state, workingDayRecord) {
    const item = state.workingRecord.find(
      (item) => item.id === workingDayRecord.id
    )
    Object.assign(item, workingDayRecord)
  }
}

export const actions = {
  fetchDayRecords(vuexContext, context) {
    return this.$axios
      .$get('workingdayrecord/')
      .then((data) => {
        const workingRecord = []
        for (const key in data) {
          workingRecord.push({ ...data[key] })
        }
        vuexContext.commit('setworkingRecord', workingRecord)
      })
      .catch((e) => context.error(e))
  },
  addDayRecord(vuexContext, dayRecord) {
    vuexContext.commit('addDayRecord', { ...dayRecord })
  },
  updateDayRecord(context, dayRecord) {
    context.commit('updateDayRecord', { ...dayRecord })
  }
}

export const getters = {
  workingRecord(state) {
    return state.workingRecord
  }
}
