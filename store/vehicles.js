/* eslint-disable no-console */
export const state = () => ({
  loadedVehicles: []
})

export const mutations = {
  setVehicles(state, vehicle) {
    state.loadedVehicles = vehicle
  }
}

export const actions = {
  fetchVehicles(vuexContext, context) {
    return this.$axios
      .$get('vehicles/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key] })
        }
        vuexContext.commit('setVehicles', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setVehicles(vuexContext, vehicle) {
    vuexContext.commit('setVehicles', vehicle)
  }
}

export const getters = {
  loadedVehicles(state) {
    return state.loadedVehicles
  }
}
