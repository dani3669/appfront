/* eslint-disable no-console */
export const state = () => ({
  loadedContractTypes: []
})

export const mutations = {
  setContractTypes(state, contractType) {
    state.loadedContractTypes = contractType
  }
}

export const actions = {
  fetchContractTypes(vuexContext, context) {
    return this.$axios
      .$get('contracttypes/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setContractTypes', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setContractTypes(vuexContext, contractType) {
    vuexContext.commit('setContractTypes', contractType)
  }
}

export const getters = {
  loadedContractTypes(state) {
    return state.loadedContractTypes
  }
}
