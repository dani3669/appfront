/* eslint-disable no-console */
export const state = () => ({
  loadedTaskTypes: []
})

export const mutations = {
  setTaskTypes(state, taskType) {
    state.loadedTaskTypes = taskType
  }
}

export const actions = {
  fetchTaskTypes(vuexContext, context) {
    return this.$axios
      .$get('tasktypes/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setTaskTypes', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setTaskTypes(vuexContext, taskType) {
    vuexContext.commit('setTaskTypes', taskType)
  }
}

export const getters = {
  loadedTaskTypes(state) {
    return state.loadedTaskTypes
  }
}
