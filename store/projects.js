export const state = () => ({
  loadedProjects: null,
  favouriteProjects: []
})

export const mutations = {
  setProjects(state, project) {
    state.loadedProjects = project
  },
  setFavouirteProjects(state, favouriteProjects) {
    state.favouriteProjects = favouriteProjects
  },
  deleteFavoritePrjoject(state, projectId) {
    state.favouriteProjects = state.favouriteProjects.filter(
      (e) => e.id !== projectId
    )
  },
  addFavoriteProject(state, newFavoritePoject) {
    state.favouriteProjects.push(newFavoritePoject)
  },
  updateProject(state, project) {
    const item = state.loadedProjects.find((item) => item.id === project.id)
    Object.assign(item, project)
  }
}

export const actions = {
  async fetchProjects(vuexContext, state) {
    if (vuexContext.state.loadedProjects) return true
    else {
      const espera = this.$axios
        .$get('favouriteprojects/')
        .then((data) => {
          const favouirteProjectsList = []
          for (const key in data) {
            favouirteProjectsList.push({ ...data[key] })
          }
          vuexContext.commit('setFavouirteProjects', favouirteProjectsList)
        })
        .catch((e) => console.log(e))
      await espera
      const favs = vuexContext.getters.loadedFavouriteProjects
      await this.$axios
        .$get('projects/')
        .then((data) => {
          const postsArray = []
          for (const key in data) {
            if (favs.some((e) => e.project === data[key].id)) {
              postsArray.push({ ...data[key], favourite: true })
            } else {
              postsArray.push({ ...data[key], favourite: false })
            }
          }
          vuexContext.commit('setProjects', postsArray)
        })
        .catch((e) => {
          console.log(e)
        })
    }
  },
  setProjects(vuexContext, project) {
    vuexContext.commit('setProjects', project)
  },
  setFavouirteProjects(state, favouriteProjects) {
    state.favouirteProjects = favouriteProjects
  },
  deleteFavoriteProject(vuexContext, projectId) {
    this.$axios.$delete('favouriteprojects/' + projectId + '/').then(() => {
      vuexContext.commit('deleteFavoritePrjoject', projectId)
    })
  },
  addFavoriteProject(vuexContext, projectId) {
    vuexContext.commit('addFavoriteProject', { ...projectId })
  },
  updateProject(vuexContext, project) {
    vuexContext.commit('updateProject', project)
  }
}

export const getters = {
  loadedProjects(state) {
    return state.loadedProjects
  },
  loadedFavouriteProjects(state) {
    return state.favouriteProjects
  }
}
