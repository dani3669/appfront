/* eslint-disable no-console */
export const state = () => ({
  loadedProjectStatus: []
})

export const mutations = {
  setProjectStatus(state, projectStatus) {
    state.loadedProjectStatus = projectStatus
  }
}

export const actions = {
  fetchProjectStatus(vuexContext, context) {
    return this.$axios
      .$get('projectstatus/')
      .then((data) => {
        const projectStatusArray = []
        for (const key in data) {
          projectStatusArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setProjectStatus', projectStatusArray)
      })
      .catch((e) => context.error(e))
  },
  setProjectStatus(vuexContext, loadedProjectStatus) {
    vuexContext.commit('setProjectStatus', loadedProjectStatus)
  }
}

export const getters = {
  loadedProjectStatus(state) {
    return state.loadedProjectStatus
  }
}
