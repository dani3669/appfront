/* eslint-disable no-console */
export const state = () => ({
  loadedAbsenceTypes: []
})

export const mutations = {
  setAbsenceTypes(state, absenceType) {
    state.loadedAbsenceTypes = absenceType
  }
}

export const actions = {
  fetchAbsenceTypes(vuexContext, context) {
    return this.$axios
      .$get('absencetypes/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setAbsenceTypes', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setAbsenceTypes(vuexContext, absenceType) {
    vuexContext.commit('setAbsenceTypes', absenceType)
  }
}

export const getters = {
  loadedAbsenceTypes(state) {
    return state.loadedAbsenceTypes
  }
}
