export const state = () => ({
  loadedRecords: null
})

export const mutations = {
  setRecords(state, records) {
    state.loadedRecords = records
  },
  updateRecord(state, record) {
    let item = state.loadedRecords.find((item) => item.id === record.id)
    item = { start: '00:00' }
    Object.assign(item, record)
  }
}

export const actions = {
  fetchRecords(vuexContext, context) {
    if (vuexContext.state.loadedRecords) {
      return true
    }
    return this.$axios
      .$get('allrecords/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key] })
        }
        vuexContext.commit('setRecords', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setRecords(vuexContext, records) {
    vuexContext.commit('setRecords', records)
  },
  updateRecord(vuexContext, record) {
    vuexContext.commit('updateRecord', record)
  }
}

export const getters = {
  loadedRecords(state) {
    return state.loadedRecords
  }
}
