/* eslint-disable no-console */
export const state = () => ({
  loggedInUser: []
})

export const mutations = {
  setLoggedInUser(state, loggedInUser) {
    state.loggedInUser = loggedInUser
  }
}

export const actions = {
  fetchLoggedInUser(vuexContext, context) {
    return this.$axios
      .$get('api/auth/user/')
      .then((data) => {
        const loggedInUser = []
        for (const key in data) {
          loggedInUser.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setLoggedInUser', loggedInUser)
      })
      .catch((e) => context.error(e))
  },
  setLoggedInUser(vuexContext, loggedInUser) {
    vuexContext.commit('setLoggedInUser', loggedInUser)
  }
}

export const getters = {
  loggedInUser(state) {
    return state.loggedInUser
  }
}
