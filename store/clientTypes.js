/* eslint-disable no-console */
export const state = () => ({
  loadedClientTypes: []
})

export const mutations = {
  setClientTypes(state, clientType) {
    state.loadedClientTypes = clientType
  }
}

export const actions = {
  fetchClientTypes(vuexContext, context) {
    return this.$axios
      .$get('clienttypes/')
      .then((data) => {
        const postsArray = []
        for (const key in data) {
          postsArray.push({ ...data[key], edit: false })
        }
        vuexContext.commit('setClientTypes', postsArray)
      })
      .catch((e) => context.error(e))
  },
  setClientTypes(vuexContext, clientType) {
    vuexContext.commit('setClientTypes', clientType)
  }
}

export const getters = {
  loadedClientTypes(state) {
    return state.loadedClientTypes
  }
}
