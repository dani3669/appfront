export const state = () => ({
  loadedAbsences: null
})

export const mutations = {
  setAbsences(state, absences) {
    state.loadedAbsences = absences
  },
  updateAbsence(state, absence) {
    const item = state.loadedAbsences.find((item) => item.id === absence.id)
    Object.assign(item, absence)
  }
}

export const actions = {
  fetchAbsences(vuexContext, context) {
    if (vuexContext.state.loadedAbsences) {
      return true
    }
    return this.$axios
      .$get('absences/')
      .then((data) => {
        const absencesList = []
        for (const key in data) {
          absencesList.push({ ...data[key] })
        }
        vuexContext.commit('setAbsences', absencesList)
      })
      .catch((e) => context.error(e))
  },
  updateAbsence(vuexContext, absence) {
    vuexContext.commit('updateAbsence', absence)
  }
}

export const getters = {
  loadedAbsences(state) {
    return state.loadedAbsences
  }
}
