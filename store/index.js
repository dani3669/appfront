export const strict = false

export const state = () => ({
  counter: 0,
  env: {},
  manager: null
})

export const mutations = {
  increment(state) {
    state.counter++
  },
  setEnv(state, env) {
    state.env = env
  },
  setManger(state, isManager) {
    state.manager = isManager
  }
}

export const actions = {
  nuxtServerInit({ dispatch, commit }, context) {
    if (process.server && context.store.$auth.user) {
      context.store.dispatch('setManager', context.store.$auth.user.is_manager)
    }
  },
  setManager(vuexContext, isManager) {
    vuexContext.commit('setManger', isManager)
  }
}

export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth
  },
  isManager(state) {
    return state.manager
  }
}
