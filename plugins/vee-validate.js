/* eslint-disable prettier/prettier */
import Vue from 'vue';
import { ValidationProvider, extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';

// Add a rule.
extend('secret', {
  validate: (value) => value === 'example',
  message: 'This is not the magic word'
});

// Override the default message.
extend('required', {
  ...required,
  message: 'This field is required'
});

extend("email", {
    ...email,
    message: "This field must be a valid email"
  });
  

// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
