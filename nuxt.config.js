import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack'
export default {
  apps: [
    {
      name: 'NuxtAppName',
      exec_mode: 'cluster',
      instances: 'max', // Or a number of instances
      script: './node_modules/nuxt/bin/nuxt.js',
      args: 'start'
    }
  ],
  server: {
    port: 8000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    titleTemplate: 'AlziNet',
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['@/assets/variables.scss'],
  /*
   ** Plugins to load before mounting the App
   */
  /*
   ** You can add configuration to webpack
   *
   */
  build: {
    extractCSS: true,
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        _: 'lodash'
      })
    ],
    analyze: false,
    transpile: ['vee-validate/dist/rules'],
    extend(config, ctx) {}
  },
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    '@nuxtjs/vuetify',
    '@nuxtjs/date-fns'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    '@nuxtjs/proxy',
    '@nuxtjs/auth'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: 'http://18.188.175.160/'
    // baseURL: 'http://192.168.1.133:8080/'
  },
  plugins: ['~/plugins/vee-validate.js'],
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: '#34421E',
          accent: colors.grey.darken3,
          secondary: '#C19434',
          info: colors.teal.lighten1,
          warning: '#ffcc00',
          error: '#9B111E',
          success: '#808000',
          third: '#F1F2EF'
        },
        light: {
          primary: '#34421E',
          accent: colors.grey.darken3,
          secondary: '#C19434',
          info: colors.teal.lighten1,
          warning: '#ffcc00',
          error: '#9B111E',
          success: '#808000',
          third: '#F1F2EF'
        }
      }
    }
  },
  /*
   ** Build configuration
   */
  /*
   ** Add your global variables here
   */
  env: {
    baseUrl: 'http://18.188.175.160/'
    // baseUrl: 'http://192.168.1.133:8080/'
  },
  router: {
    middleware: ['auth']
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: '/api/token',
            method: 'post',
            propertyName: 'token'
          },
          logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/api/auth/user/', method: 'get' }
        },
        // tokenRequired: true,
        tokenType: 'bearer',
        autoFetchUser: false
      }
    },
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/login',
      home: '/dailyRecord'
    }
  },
  proxy: {
    '/api': {
      target: 'http://18.188.175.160/',
      // target: 'http://192.168.1.133:8080/',
      pathRewrite: {
        '^/api': '/'
      }
    }
  }
}
